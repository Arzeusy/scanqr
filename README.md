# Generador y lector de codigo QR

esta aplicacion scanea codigo QR o lo genera a partir de una cadena

## Requerimientos 🚀
node 10.9
ionic 3.20
mpm 6.2.0
cordova 7.0


### Instalación 🔧
```
npm install
```

```
ionic cordova platform add andorid
```

```
ionic cordoba run android
```

### Correccion de errores de cordova-android ⚙️
```
Error: No resource found that matches the given name (at 'networkSecurityConfig' with value '@xml/network_security_config').
```

**Resuelto:**
is un error con las versiones viejas de cordova-android

En el archivo config.xml
```
     <preference name="SplashScreen" value="screen" />
     <preference name="SplashScreenDelay" value="3000" />
     <platform name="android">
-        <edit-config file="app/src/main/AndroidManifest.xml" mode="merge" target="/manifest/application" xmlns:android="http://schemas.android.com/apk/res/android">
+        <edit-config file="AndroidManifest.xml" mode="merge" target="/manifest/application" xmlns:android="http://schemas.android.com/apk/res/android">
             <application android:networkSecurityConfig="@xml/network_security_config" />
         </edit-config>
-        <resource-file src="resources/android/xml/network_security_config.xml" target="app/src/main/res/xml/network_security_config.xml" />
+        <resource-file src="resources/android/xml/network_security_config.xml" target="res/xml/network_security_config.xml" />
         <allow-intent href="market:*" />
         <icon density="ldpi" src="resources/android/icon/drawable-ldpi-icon.png" />
         <icon density="mdpi" src="resources/android/icon/drawable-mdpi-icon.png" />
```