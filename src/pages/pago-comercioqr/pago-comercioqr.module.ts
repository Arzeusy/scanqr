import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagoComercioqrPage } from './pago-comercioqr';

@NgModule({
  declarations: [
    PagoComercioqrPage,
  ],
  imports: [
    IonicPageModule.forChild(PagoComercioqrPage),
  ],
})
export class PagoComercioqrPageModule {}
