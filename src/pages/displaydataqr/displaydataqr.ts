import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the DisplaydataqrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-displaydataqr',
  templateUrl: 'displaydataqr.html',
})
export class DisplaydataqrPage {
  textoJson = "";
  qrData = {
    "comercio":"",
    "monto": "100",
    "cuenta":"123456789",
    "concepto":"esto es un pago generico",
    "token":"65498325895",
    "moneda": "GTQ"
  };
  _stateError = true;
  esTransferencia = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl:ToastController) {
    this.textoJson = this.navParams.get('text');
    if (this.navParams.get('esTransferencia')) {
      this.esTransferencia = this.navParams.get('esTransferencia');
    } else { this.esTransferencia = 0; }
    console.log(this.textoJson)
    if( this.textoJson){
      this.qrData = JSON.parse(this.textoJson);
    }
    
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad DisplaydataqrPage');
    if ( !this.qrData.comercio  && this.esTransferencia == 0) {
      console.log("el codigo no es para Comercios")
      this._stateError = true;
      this.showToastWithCloseButton("el codigo no es para Comercios")
    } else if ( this.qrData.comercio  && this.esTransferencia == 1) {
      console.log("el codigo no es para Transferencias")
      this._stateError = true;
      this.showToastWithCloseButton("el codigo no es para Transferencias")
    } else {
      this._stateError = false;
    }
  }

  return(){
    this.navCtrl.goToRoot({});
  }


  showToastWithCloseButton( message:string) {
    const toast = this.toastCtrl.create({
      message: message,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present()
    toast.onWillDismiss((_null, role) => {
      switch (role) {
        case 'close':
          this.navCtrl.goToRoot({})
          break;
        case 'backdrop':
          console.log("Duration timeout");
          break;
        case 'custom':
          console.log("toast.dismiss('custom'); called");
          break;
      }
    });
  }

}
