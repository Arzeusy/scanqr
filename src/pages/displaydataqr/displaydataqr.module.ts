import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplaydataqrPage } from './displaydataqr';

@NgModule({
  declarations: [
    DisplaydataqrPage,
  ],
  imports: [
    IonicPageModule.forChild(DisplaydataqrPage),
  ],
})
export class DisplaydataqrPageModule {}
