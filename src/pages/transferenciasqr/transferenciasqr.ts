import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EncoderqrPage } from '../encoderqr/encoderqr';
import { ScanerqrPage } from '../scanerqr/scanerqr';
import { Decoder } from '@nuintun/qrcode';
import { DisplaydataqrPage } from '../displaydataqr/displaydataqr';
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ToastController } from 'ionic-angular';
import { PagoComercioqrPage } from '../pago-comercioqr/pago-comercioqr';

const qrcodeDecoder = new Decoder();
/**
 * Generated class for the TransferenciasqrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transferenciasqr',
  templateUrl: 'transferenciasqr.html',
})
export class TransferenciasqrPage {
  hasReadAcces:boolean = false;
  options :any={}
  _messageError:string = "";

  imageURI:any;
  imageFileName:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private androidPermissions: AndroidPermissions,
    private camera: Camera,
    private toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad TransferenciasqrPage');
  }

  ionViewWillEnter() {
    this.checkPermissionsRead();
  }

  scanerqr(){
    this.navCtrl.push(ScanerqrPage, {"esTransferencia":1});
  }
  pagarqr(){
    this.navCtrl.push(PagoComercioqrPage)
  }

  encoderqr(){
    this.navCtrl.push(EncoderqrPage);
  }

  checkPermissionsRead() {
    this.androidPermissions
    .checkPermission(this.androidPermissions
    .PERMISSION.READ_EXTERNAL_STORAGE)
    .then((result) => {
      console.log('Has permission?',result.hasPermission);
      this.hasReadAcces = result.hasPermission;
    },(err) => {
        this.androidPermissions
          .requestPermission(this.androidPermissions
          .PERMISSION.READ_EXTERNAL_STORAGE);
    });
    if (!this.hasReadAcces) {
      this.androidPermissions
        .requestPermissions([this.androidPermissions
        .PERMISSION.READ_EXTERNAL_STORAGE]);
    }
  }


  decodeQRImage() {
    if (!this.hasReadAcces){
      this.checkPermissionsRead()
    }
    if (this.hasReadAcces){
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      }
      this.camera.getPicture(options).then((imageData) => {
        this.imageURI = imageData;
        qrcodeDecoder.scan(imageData)
        .then(result => {
          console.log(result.data);
          this.navCtrl.push(DisplaydataqrPage, { text:result.data, esTransferencia:1})
        })
        .catch(error => {
          console.error(error);
        });
      }, (err) => {
        console.log(err);
        this.showToastWithCloseButton(err);
      });
    
      
    }
  }

  showToastWithCloseButton( message:string) {
    const toast = this.toastCtrl.create({
      message: message,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present()
  }

}
