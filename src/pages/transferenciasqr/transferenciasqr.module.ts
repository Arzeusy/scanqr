import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransferenciasqrPage } from './transferenciasqr';

@NgModule({
  declarations: [
    TransferenciasqrPage,
  ],
  imports: [
    IonicPageModule.forChild(TransferenciasqrPage),
  ],
})
export class TransferenciasqrPageModule {}
