import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncoderqrPage } from './encoderqr';

@NgModule({
  declarations: [
    EncoderqrPage,
  ],
  imports: [
    IonicPageModule.forChild(EncoderqrPage),
  ],
})
export class EncoderqrPageModule {}
