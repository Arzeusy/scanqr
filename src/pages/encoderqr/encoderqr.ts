import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import QRCode from 'qrcode';
import { Base64ToGallery, Base64ToGalleryOptions } from '@ionic-native/base64-to-gallery';
import { AndroidPermissions } from "@ionic-native/android-permissions";
/**
 * Generated class for the EncoderqrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encoderqr',
  templateUrl: 'encoderqr.html',
})
export class EncoderqrPage {
  qrData = {
    "comercio":"",
    "monto": null,
    "cuenta":"",
    "concepto":"",
    "token":"",
    "moneda": "GTQ"
  };
  qrcodeCreatedCode = null;
  error ="";
  hasWriteAccess: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private base64ToGallery: Base64ToGallery,
    private androidPermissions: AndroidPermissions) {
  }

  ionViewWillEnter() {
    this.checkPermissions();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad EncoderqrPage');
  }

  cancel(){
    this.navCtrl.pop()
  }

  getToken(){
    function uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }
    console.log(uuidv4());
  }
  
  
  process() {
    const self = this;

    function uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }
    self.qrData.token = uuidv4();
    
    let jsonCadena = JSON.stringify(self.qrData)
    console.log(jsonCadena)
    QRCode.toDataURL(jsonCadena, { errorCorrectionLevel: 'H' }, function (err, url) {
      self.qrcodeCreatedCode = url;
      console.log(self.qrData)
    })
  }

  displayQrCode() {
    return this.qrcodeCreatedCode !== null;
  }


  checkPermissions() {
    this.androidPermissions
    .checkPermission(this.androidPermissions
    .PERMISSION.WRITE_EXTERNAL_STORAGE)
    .then((result) => {
      console.log('Has permission?',result.hasPermission);
      this.hasWriteAccess = result.hasPermission;
    },(err) => {
        this.androidPermissions
          .requestPermission(this.androidPermissions
          .PERMISSION.WRITE_EXTERNAL_STORAGE);
    });
    if (!this.hasWriteAccess) {
      this.androidPermissions
        .requestPermissions([this.androidPermissions
        .PERMISSION.WRITE_EXTERNAL_STORAGE]);
    }

    // this.base64ToGallery.base64ToGallery(this.qrcodeCreatedCode, { prefix: '_img', mediaScanner: false }).then(
    //   res => {this.error=res;},
    //   err => {this.error=" err" + err;}
    // ).catch(
    //   e => {this.error=e;}
    // );
  }

  download() {
    if (!this.hasWriteAccess) {
      this.checkPermissions();
    }
    if (this.hasWriteAccess){
      let options: Base64ToGalleryOptions = {
        prefix: 'img_QRcodeBanrural_', 
        mediaScanner: true
      };
      this.base64ToGallery.base64ToGallery(this.qrcodeCreatedCode, options).then(
        res => {this.error=res;},
        err => {this.error=" err" + err;}
      ).catch(
        e => {this.error=e;}
      );
    }
  }

}
