import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanerqrPage } from './scanerqr';

@NgModule({
  declarations: [
    ScanerqrPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanerqrPage),
  ],
})
export class ScanerqrPageModule {}
