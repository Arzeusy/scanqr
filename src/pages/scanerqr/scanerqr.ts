import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { DisplaydataqrPage } from '../displaydataqr/displaydataqr';

/**
 * Generated class for the ScanerqrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scanerqr',
  templateUrl: 'scanerqr.html',
})
export class ScanerqrPage {
  text: string = "no hay data";
  esTransferencia = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams, private qrScanner: QRScanner) {
    let paramTransfer = navParams.get('esTransferencia');
    if(paramTransfer){
      this.esTransferencia = paramTransfer;
    }else{
      this.esTransferencia = 0
    }
  }

  ionViewDidLoad() {
   
      console.log("console log importante android x2----------------------------->",)
      this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
         if (status.authorized) {
           // camera permission was granted
          console.log("entre al scaneo autorizado----------------------------->",)
          
          // this.mostrarScaner = true;
          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            this.text = text;
            this.qrScanner.hide(); // hide camera preview
            //  this.mostrarScaner = false;
            scanSub.unsubscribe(); // stop scanning
          });
          this.qrScanner.resumePreview();

          // show camera preview
          this.qrScanner.show()
          .then((data : QRScannerStatus)=> { 
            console.log("data----------------------------->", data.showing)
            // alert(data.showing);
          },err => {
            console.log("err----------------------------->", err)
            // alert(err);

          });
          console.log("entre al scaneo autorizado x2----------------------------->",)
          
         } else if (status.denied) {
             console.log(' camera permission was permanently denied');
             // camera permission was permanently denied
           // you must use QRScanner.openSettings() method to guide the user to the settings page
           // then they can grant the permission from there
         } else {
             console.log(' camera error');
             // permission was denied, but not permanently. You can ask for permission again at a later time.
         }
      })
      .catch((e: any) => console.log('Error is -------------->', e));
      console.log("consolertante android x2 final ----------------------------->",)

    

  }




  barcodeScan(){
    let scanSub = this.qrScanner.scan().subscribe((text: string) => {
      console.log('Scanned something', text);
      this.text = text;
      this.navCtrl.push(DisplaydataqrPage, {text, esTransferencia:this.esTransferencia})
      this.qrScanner.hide(); // hide camera preview
      //  this.mostrarScaner = false;
      scanSub.unsubscribe(); // stop scanning
    });
    this.qrScanner.resumePreview();

    // show camera preview
    this.qrScanner.show()
    .then((data : QRScannerStatus)=> { 
      console.log("data----------------------------->", data.showing)
      // alert(data.showing);
    },err => {
      console.log("err----------------------------->", err)
      // alert(err);

    });
    console.log("entre al scaneo autorizado x2----------------------------->",)
  }


}
