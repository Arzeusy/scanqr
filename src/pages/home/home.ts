import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import QRCode from 'qrcode';
import { Platform } from 'ionic-angular';
import { ScanerqrPage } from '../scanerqr/scanerqr';
import { EncoderqrPage } from '../encoderqr/encoderqr';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  qrData = null;
  createdCode = null;
  encodedData: any = {};
  scannedData: any = {};
  href = "";

  mostrarScaner = false;

  qrcodeCreatedCode = null;
  constructor(public navCtrl: NavController, public platform:Platform) {

  }

  GenerarCodigo(){
      this.createdCode = this.qrData;
  }

  downloadImage(){
    // let elemento = document.getElementsByClassName('qrcode')[0].children;
    // this.href = elemento[0].src;
    // console.log(this.href)
  }

  displayQrCode() {
    return this.qrcodeCreatedCode !== null;
  }
  process() {
    const self = this;
    QRCode.toDataURL(self.qrData, { errorCorrectionLevel: 'H' }, function (err, url) {
      self.qrcodeCreatedCode = url;
      console.log(self.qrData)
    })
  }

  scanerqr(){
    this.navCtrl.push(ScanerqrPage);
  }

  encoderqr(){
    this.navCtrl.push(EncoderqrPage);
  }
}
