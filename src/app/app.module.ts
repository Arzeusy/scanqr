import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import {NgxQRCodeModule} from 'ngx-qrcode2';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QRScanner } from '@ionic-native/qr-scanner';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Camera } from '@ionic-native/camera';

import { ScanerqrPage } from '../pages/scanerqr/scanerqr';
import { EncoderqrPage } from '../pages/encoderqr/encoderqr';
import { DisplaydataqrPage } from '../pages/displaydataqr/displaydataqr';
import { TransferenciasqrPage } from '../pages/transferenciasqr/transferenciasqr';
import { PagoComercioqrPage } from '../pages/pago-comercioqr/pago-comercioqr';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ScanerqrPage,
    EncoderqrPage,
    DisplaydataqrPage,
    TransferenciasqrPage,
    PagoComercioqrPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ScanerqrPage,
    EncoderqrPage,
    DisplaydataqrPage,
    TransferenciasqrPage,
    PagoComercioqrPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QRScanner,
    Base64ToGallery,
    AndroidPermissions,
    Camera
  ]
})
export class AppModule {}
